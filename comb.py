import pandas as pd
import numpy as np


def apply_comb(sample_runs, test, norma, comb_type):
    r = []
    result = []

    for i in range(len(sample_runs)):
        run_data = pd.read_csv(sample_runs[i], sep="\t", names=["topic_id", "fixed", "doc_id", "rank", "score", "run_id"])
        r.append(run_data)

    # for the first two runs, full-join based on the topic and doc id (those of the second run will be identified by a +)
    merged = pd.merge(r[0], r[1], right_on=["topic_id", "doc_id"], left_on=["topic_id", "doc_id"], how="outer", suffixes=("", "+"))
    merged = merged[["topic_id", "doc_id", "score", "score+"]]
    # do the same for next runs, considering the one obtained previously as first
    for rr in r[2:]:
        merged = pd.merge(merged, rr, right_on=["topic_id", "doc_id"], left_on=["topic_id", "doc_id"], how="outer", suffixes=("", "+"))
        merged = merged[["topic_id", "doc_id", "score", "score+"]]

    # sum the scores obtained and order them by topic and decreasing score
    if comb_type == 'MNZ':
        merged["non_zero_rels"] = merged[["score", "score+"]].fillna(0).apply(np.count_nonzero, raw=True, axis=1)   # count non-zero (excluding NaN, i.e. NaN = 0)

    if norma == "zmuv":  # NaN = -2 --> value of the unretrieved for zmuv
        merged = merged.fillna(-2)    
    merged["sum_score"] = merged[["score", "score+"]].apply(np.nansum, raw=True, axis=1)  # nansum sum (by row, axis=1) considering NaN = 0 --> are the unretrieved (only if the norm isn't zmuv there are still nan)
    
    if comb_type == 'MNZ':
        merged["sum_scoreMNZ"] = merged[["sum_score", "non_zero_rels"]].apply(np.product, raw=True, axis=1)
        merged["sum_score"] = merged["sum_scoreMNZ"]
    merged.sort_values(["topic_id", "sum_score"], ascending=[True, False], inplace=True)

    # for each different topic
    for topic in merged['topic_id'].unique():
        merged_topic = merged[merged['topic_id'] == topic]  # subset with the same topic

        # assign the rank to the documents of the same topic according to the score (rank 0 to the highest score)
        for rank, column in enumerate(merged_topic[["doc_id", "sum_score"]].head(1000).values, start=0):  # trec returns runs of a maximum of 1000 documents per topic, so we also impose for this combined
            # create the resulting run as combination
            result.append([topic, 'Q0', column[0], rank, column[1], 'comb_' + comb_type])

    # combined run
    df = pd.DataFrame(result, columns=["topic_id", "fixed", "doc_id", "rank", "score", "run_id"])

    return df
