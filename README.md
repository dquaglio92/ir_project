# Reperimento dell'informazione - 2019/2020

## IR - informatici in prima linea

* Irrera Ornella	1197668
* Mattarolo Elena	1189926
* Quaglio Davide	1202490
* Salmistraro Gianmarco	1207716

Relevance Score Normalization for Metasearch
----------------------------------------------
This repository contains the source code needed to reproduce the results of the paper:
	
	"Relevance Score Normalization for Metasearch", by Mark Montague and Javen A. Aslam in 2001.

The files organization is the following:

* in *data* folder are stored all the data needed for the computation,
	* the original runs for each trec dataset in the subfolders `/runs/trecX.results.input`, with X = {3, 5, 5subset, 9}
	* their respective normalisations in `/runs/trecX.results.input.norm` (with the name of the norm as suffix of run's name)
	* and the qrels files for each set data in `/qrelsX`
* in *plot* folder all the plots obtained are saved and organized in subfolders according to the type of combination applied
* `standard.py` `sum.py` `zmuv.py` `dmuv.py` `tanh.py` are the python code files that implement the normalization algorithms
* `comb.py` implements the combination step of the metasearch, applied one of combSUM or combMNZ method
* `ap.py` returns the mean average precision of a run
* `main.py` is the entry point of the program 

By default it computes the plot for the TREC3 dataset, 
with the combination methods that is provided in input (answer *SUM* to use combSUM and *MNZ* for combMNZ). 
For the other plots, follow the instructions in the comments inside the codes main.py and qrels.py.

**!! warning: the computation takes a significant amount of time **

