import pandas as pd
import numpy as np


def AP(run, qrels):
    # insert judgments in the run
    run_jud = pd.merge(run, qrels, right_on=["topic_id", "doc_id"], left_on=["topic_id", "doc_id"], how="left")
    run_jud = run_jud[["topic_id", "doc_id", "judgement"]]

    n_topic = len(run_jud['topic_id'].unique())  # number of different topics

    map = 0
    for topic in run_jud['topic_id'].unique():
        same_topic = run_jud[run_jud['topic_id'] == topic]  # subset with the same topic

        # recall base: number of documents judged relevant for that topic
        RB = qrels[qrels['topic_id'] == topic]['judgement'].sum()                       #so it sum the judgments instead of counting the number of relevants
        #RB = sum(1 for i in qrels[qrels['topic_id']==topic]['judgement'] if i > 0)     #so it would actually count how many relevant

        judgement = np.array(same_topic["judgement"])  # column with judgments
        rank = np.array([i for i, x in enumerate(judgement) if x > 0])  # positions in the rank where there are the relevants (! here first position is 0)

        if rank.any():  # if relevant documents were found in the run..
            ap = np.nansum(np.cumsum(judgement[rank]) / (rank + 1)) / RB  # compute the average precision for a single topic
        else:
            ap = 0

        map = map + ap / n_topic  # mean average precision: average on all topics

    return map
