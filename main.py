import glob
import os
import time
import math
from random import sample
import matplotlib.pyplot as plt
import pandas as pd
import multiprocessing as mp

from standard import standard
from sum import sum_norm
from zmuv import zmuv
from tanh import tanh
from dmuv import dmuv
from ap import AP
from comb import apply_comb
from qrels import qrels

map_run_values = []

def append_value(result):
    map_run_values.append(result)


def method_name(i, files, n, norma, comb_type):
    sample_runs = sample(files, n)  # randomly select n runs
    # take into account the best MAP value of all the runs that will be combined
    if norma == 'best':
        map_best = 0
        for k in range(n):
            sample_run = pd.read_csv(sample_runs[k], sep="\s+", names=["topic_id", "fixed", "doc_id", "rank", "score", "run_id"])
            df = pd.DataFrame(sample_run, columns=["topic_id", "fixed", "doc_id", "rank", "score", "run_id"])

            v_map = AP(df, qrels())
            if v_map > map_best:  # updating
                map_best = v_map
        return map_best
    # combine the runs and calculate MAP value of the new overall run
    run = apply_comb(sample_runs, i, norma, comb_type)
    map = AP(run, qrels())    
    return map
 

if __name__ == '__main__':
    norme = ['standard', 'sum', 'zmuv', "best"]
    #norme = ['standard', 'tanh', '2muv', "best"]       ## for the third plot
    #norme = ['standard', 'tanh', 'zmuv', "best"]       ## for PLUS
    lines_settings = [['--', 'o', 'm'], ['--', 'v', 'y'], ['--', '*', 'k'], ['--', 'x', 'b']]

    comb_type = input('Quale combinazione vuoi usare?  ')  ## comment this to plots MNZ[standard sum]+SUM[zmuv]+best  &  MNZ[standard]+SUM[tanh zmuv]+best

    ## uncomment these only the first time you want to create files with normalized scores
    # standard()
    # sum_norm()
    # zmuv()
    # dmuv()
    # tanh()

    # consider files normalized with each of the norms in the list 'norme' 
    folder = os.path.join('data', 'runs', 'trec3.results.input.norm')       ## or 'trec5.results.input.norm' 'trec5subset.results.input.norm' 'trec9.results.input.norm'
    best_folder = os.path.join('data', 'runs', 'trec3.results.input')       ## or 'trec5.results.input' 'trec5subset.results.input' 'trec9.results.input'
    for idx, norma in enumerate(norme):
        norma_time = time.time()
        files = glob.glob(folder + '/' + norma + '_*')
        if norma == 'best':
            files = glob.glob(best_folder + '/*')
        n_run = len(files)

        ## uncomment for the last plot: MNZ[standard sum]+ SUM[zmuv]+ best
        # if norma =='standard' or norma == 'sum':
        #     comb_type = 'MNZ'
        # else:
        #     comb_type = 'SUM'

        ## uncomment for the PLUS plot: MNZ[standard]+ SUM[tanh zmuv]+ best
        # if norma =='standard':
        #     comb_type = 'MNZ'
        # else:
        #     comb_type = 'SUM'

        avg_steps = []
        for n in range(2, 13, 2):  # consider a subset of runs of size n                                          #--range(2, 12, 2) for trec5subset
            n_time = time.time()
            num_test = 200  # number of repetitions on which the result will be averaged
            #num_test = int(min(200, math.factorial(n_run)/( math.factorial(n)* math.factorial(n_run-n) ) ))      ## uncomment only for trec5subset

            map_run_values = []
            pool = mp.Pool(mp.cpu_count())
            for i in range(1, num_test + 1):
                pool.apply_async(method_name, args=(i, files, n, norma, comb_type,), callback=append_value)
            pool.close()
            pool.join()
            avg_steps.append(sum(map_run_values) / len(map_run_values))

            print('completed %s step %d' % (norma, n))
            print("--- %s seconds ---" % (time.time() - n_time))
        print('completed %s' % norma)
        print("--- %s seconds ---" % (time.time() - norma_time))
        # here you can plot the graph
        plt.plot(range(2, 13, 2), avg_steps, linestyle=lines_settings[idx][0], marker=lines_settings[idx][1],    
                 color=lines_settings[idx][2], label=norma)                                                       #--range(2, 12, 2) for trec5subset

        # plot data saving 
        f = open('trec3.' + norma + '.txt', 'w')    # change trec3 with one between trec5,trec5subset,trec9 depending on the dataset considered above (lines 59,60)
        for valore in avg_steps:
            f.write("%s \n" % valore)
        f.close()
        
    plt.legend(framealpha=1, frameon=True, loc='upper left')                                                                      
    plt.show()
