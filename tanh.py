import os
import glob
import pandas as pd
import numpy as np

def tanh():
    folder = os.path.join('data', 'runs', 'trec3.results.input')    ##or 'trec5.results.input' 'trec5subset.results.input' 'trec9.results.input'
    files = glob.glob(folder + '/*')
 
    for i in range(len(files)):
        run_data = pd.read_csv(files[i], sep="\s+", names=["topic_id", "fixed", "doc_id", "rank", "score", "run_id"])
        run_data.sort_values(["topic_id", "score"], inplace=True, ascending=[True, False])
 
        mean = run_data["score"].mean()
        var = run_data["score"].std()
 
        # TANH NORM
        for s in range(len(run_data["score"])):
            run_data["score"][s] = 0.5 * (np.tanh(0.01 * ((run_data["score"][s] - mean) / var)) + 1)

        file_name = os.path.basename(files[i])

        # save the result in a new file inside the trecX.results.input.norm folder
        df = pd.DataFrame(run_data)
        df.to_csv('./data/runs/trec3.results.input.norm/tanh_' + file_name, sep='\t', header=False, index=False)    ##or 'trec5.results.input.norm' 'trec5subset.results.input.norm' 'trec9.results.input.norm'
