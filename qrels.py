import os
import glob
import pandas as pd


def qrels():
    # path to the qrels files
    folder = os.path.join('data', 'qrels3')  ##or qrels5, qrels5subset, qrels9 depending on the dataset considered in main.py (lines 59,60)
    files = glob.glob(folder + '/*')

    for i in range(len(files)):
        # read file
        run_data = pd.read_csv(files[i], sep="\s+", names=["topic_id", "fixed", "doc_id", "judgement"])

        if i == 0:
            qrels = run_data
        else:
            qrels = pd.concat([qrels, run_data])

    return qrels

